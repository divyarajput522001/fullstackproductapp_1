import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Adminlogin } from 'src/app/models/adminlogin';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {

  constructor(private router:Router) { 
    console.log('within login Componants');
    }

  ngOnInit(): void {
  }

  adminlogin(adminlogin:Adminlogin){
    if(adminlogin.username=="Admin"&&adminlogin.password=="admin"){
      console.log("Admin login Sucess")
      Swal.fire(
        'Admin Login',
        'Admin Login success',
        'success'
      )
      this.router.navigateByUrl("/admindashboard")
    }
    else{
      console.log("Login failed");
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
        })
    }   
    
  }};
